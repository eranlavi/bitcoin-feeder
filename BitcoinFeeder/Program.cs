﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using BtcE;
using System.Threading;
using System.Timers;

namespace BitcoinFeeder
{
    class Program
    {
        private static string LogFile = ConfigurationManager.AppSettings["LogFile"].Replace(".txt", "_" + string.Format("{0:dd/MM/yyyy}", DateTime.Now).Replace("/", "").Replace("\\", "") + ".txt");
        private static UnifeedServer uf;

        static CancellationTokenSource tokenSource;
        static CancellationToken ct;

        static void Main(string[] args)
        {
            String assemblyLocationFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (String.Compare(Environment.CurrentDirectory, assemblyLocationFolder, StringComparison.OrdinalIgnoreCase) != 0)
            {
                Environment.CurrentDirectory = assemblyLocationFolder;
            }

            DirectoryInfo dir = new DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["LogFile"]);
            if (!dir.Parent.Exists)
                dir.Parent.Create();

            Log("Starting PandaTS Bitcoin Feeder");

            uf = new UnifeedServer();
            uf.ServerNotification += uf_ServerNotification;
            uf.Start();

            tokenSource = new CancellationTokenSource();
            ct = tokenSource.Token;

            Thread trd = new Thread(new ThreadStart(Start));
            trd.IsBackground = true;
            trd.Start();
                       
            System.Threading.Thread.Sleep(Convert.ToInt32(ConfigurationManager.AppSettings["SelfKillMSec"].ToString()));

            tokenSource.Cancel();

            Thread.Sleep(5000);

            Log("Exiting Bitocoin Feeder");
        }

        static void Start()
        {
            while (!ct.IsCancellationRequested)
            {
                try
                {
                    Ticker ticker = BtceApi.GetTicker(BtcePair.btc_usd);
                    if (ticker != null)
                    {
                        uf.SendData(ConfigurationManager.AppSettings["BTCUSDoutputName"] + " " + ticker.ServerTime + " " + ticker.Sell + " " + ticker.Buy + "\r\n");
                    }
                }
                catch (Exception ex)
                {
                    Log("Exception on " + ConfigurationManager.AppSettings["BTCUSDoutputName"] + " request: " + ex.Message);
                }

                try
                {
                    Ticker ticker = BtceApi.GetTicker(BtcePair.btc_eur);
                    if (ticker != null)
                    {
                        uf.SendData(ConfigurationManager.AppSettings["BTCEURoutputName"] + " " + ticker.ServerTime + " " + ticker.Sell + " " + ticker.Buy + "\r\n");
                    }
                }
                catch (Exception ex)
                {
                    Log("Exception on " + ConfigurationManager.AppSettings["BTCEURoutputName"] + " request: " + ex.Message);
                }

                Thread.Sleep(Convert.ToInt32(ConfigurationManager.AppSettings["PollingTimerMSec"].ToString()));
            }
           
        }
        
        static void uf_ServerNotification(string obj)
        {
            Log(obj);
        }

        static Object _threadLocker = new Object();
        public static void Log(string data)
        {
            lock (_threadLocker)
            {
                try
                {
                    string log = ConfigurationManager.AppSettings["LogFile"].Replace(".txt", "_" + string.Format("{0:dd/MM/yyyy}", DateTime.Now).Replace("/", "").Replace("\\", "") + ".txt");
                    if (log != LogFile)
                        LogFile = log;

                    string date = string.Format("{0:MM/dd/yyyy H:mm:ss:fff} ", DateTime.Now);

                    try
                    {

                        System.IO.File.AppendAllText(LogFile, date + data + "\r\n");
                    }
                    catch { }
                }
                catch { }
            }
        }


    }
}
