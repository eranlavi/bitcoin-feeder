﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Threading;

namespace BitcoinFeeder
{
    public class UnifeedServer
    {
        public class StateObject
        {
            public Socket workSocket = null;
            public const int BUFFER_SIZE = 1024;
            public byte[] buffer = new byte[BUFFER_SIZE];
            public string sb = string.Empty;
            public int WritePendingBytes = 0;
            public bool Valid = false;
        }

        public event Action<string> ServerNotification;
        int BackLog = 1024 * 10;
       // ArrayList connections = new ArrayList();
        ConcurrentDictionary<int, StateObject> connections = new ConcurrentDictionary<int, StateObject>();
        

        int MaxPendingWriteBufferToClient = Convert.ToInt32(ConfigurationManager.AppSettings["MaxPendingWriteBufferToClient"]);
        public UnifeedServer()
        {
            
        }

        public void Start()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartServer), null);
        }

        void StartServer(Object stateInfo)
        {
            try
            {
                IPEndPoint localEP = new IPEndPoint(/*IPAddress.Parse(ConfigurationManager.AppSettings["Server"])*/IPAddress.Any, Convert.ToInt32(ConfigurationManager.AppSettings["Port"]));

                Socket listener = new Socket(localEP.Address.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                listener.Bind(localEP);
                listener.Listen(BackLog);

                listener.BeginAccept(
                        new AsyncCallback(acceptCallback),
                        listener);
            }
            catch (Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("StartServer() exception: " + ex.Message);
            }         
        }

        void DisconnectClient(StateObject so)
        {
            try
            {
                connections.TryRemove((int)so.workSocket.Handle, out so);
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("DisconnectClient() exception on remove from list: " + ex.Message);
            }

            try
            {
                if (ServerNotification != null)
                    ServerNotification("Client " + IPAddress.Parse(((IPEndPoint)so.workSocket.RemoteEndPoint).Address.ToString()) + " disconnected");
                //Console.WriteLine(DateTime.Now.ToString() + " Client: " + IPAddress.Parse(((IPEndPoint)so.workSocket.RemoteEndPoint).Address.ToString()) + " disconnected");
                so.workSocket.Close();
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("DisconnectClient() exception on close socket: " + ex.Message);
            }

            
        }

        void acceptCallback(IAsyncResult ar)
        {
            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            try
            {                
                if (ServerNotification != null)
                    ServerNotification("Got new connection from: " + IPAddress.Parse(((IPEndPoint)handler.RemoteEndPoint).Address.ToString()));

                //Console.WriteLine(DateTime.Now.ToString() + " Got new connection from: " + IPAddress.Parse(((IPEndPoint)handler.RemoteEndPoint).Address.ToString()));

                handler.NoDelay = true;
                //handler.SendBufferSize = 512;

                StateObject so = new StateObject();
                so.workSocket = handler;
                //so.sa.UserToken = 0;
                
                //connections.Add(handler);

                string auth = "Login: ";
                byte[] arr;
                int len;
                if (ConfigurationManager.AppSettings["Authenticate"] == "true")
                {
                    System.IO.StreamReader Reader = new System.IO.StreamReader(new NetworkStream(handler, false));

                    string[] d = System.IO.File.ReadAllLines("Security.txt");
                    byte[] r = new byte[128];
                    string ret;
                    handler.ReceiveTimeout = 10000;

                    auth = "Login: ";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);
                    ret = Reader.ReadLine();
                    if (d[0] != ret)
                    {
                        handler.Close();
                        Reader.Close();
                        Reader.Dispose();
                        return;
                    }

                    auth = "Password: ";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);
                    ret = Reader.ReadLine();
                    if (d[1] != ret)
                    {
                        handler.Close();
                        Reader.Close();
                        Reader.Dispose();
                        return;
                    }

                    auth = "Access granted";
                    arr = System.Text.Encoding.ASCII.GetBytes(auth);
                    len = handler.Send(arr);
                }
                
                
                //handler.BeginSend(arr, 0, arr.Length, 0,
                //                            new AsyncCallback(SendCallback), so);

                //auth = "Password: ";
                //arr = System.Text.Encoding.ASCII.GetBytes(auth);
                //handler.BeginSend(arr, 0, arr.Length, 0,
                //                            new AsyncCallback(SendCallback), so);

                //auth = "Access granted: ";
                //arr = System.Text.Encoding.ASCII.GetBytes(auth);
                //handler.BeginSend(arr, 0, arr.Length, 0,
                //                            new AsyncCallback(SendCallback), so);

                so.Valid = true;
                connections.TryAdd((int)so.workSocket.Handle, so);

                handler.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0,
                           new AsyncCallback(ReadCallback), so);
                               
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("acceptCallback() exception: " + ex.Message);

                try
                {
                    handler.Close();
                }
                catch { }                
            }
            finally
            {
                listener.BeginAccept(
                            new AsyncCallback(acceptCallback),
                            listener); 
            }
        }

        private void ReadCallback(IAsyncResult ar)
        {
            StateObject so = (StateObject)ar.AsyncState;
            Socket s = so.workSocket;

            try
            {                
                int read = s.EndReceive(ar);

                if (read > 0)
                {
                    so.sb = Encoding.ASCII.GetString(so.buffer, 0, read);
                }
                else
                {
                    so.Valid = false;

                    DisconnectClient(so);
                }
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("ReadCallback() exception: " + ex.Message);
            }
            finally
            {
                /*if(null != s && s.Connected)
                    s.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0,
                                             new AsyncCallback(ReadCallback), so);*/
                try
                {
                    if (null != s && s.Connected && so.Valid)
                        s.BeginReceive(so.buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReadCallback), so);
                    else
                    {
                        if (null != s && s.Connected && so.Valid)
                            DisconnectClient(so);
                    }

                }
                catch
                {
                    DisconnectClient(so);
                }
            }
        }

        private ReaderWriterLockSlim PublishLock = new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
        public void SendData(/*string Symbol, string dt, string Bid, string Ask*/string data)
        {
            try
            {
                //string msgToSend = string.Format("{0} {1} {2} {3}\r\n", Symbol, dt, Bid, Ask);
                //byte[] bindata = System.Text.Encoding.ASCII.GetBytes(msgToSend);
                byte[] bindata = System.Text.Encoding.ASCII.GetBytes(data);

                if (PublishLock.TryEnterWriteLock(50))
                {
                    try
                    {
                        IEnumerator e = connections.GetEnumerator();
                        while (e.MoveNext())
                        {
                            KeyValuePair<int, StateObject> s = (KeyValuePair<int, StateObject>)e.Current;
                            StateObject so = (StateObject)s.Value; //(StateObject)e.Current;

                            try
                            {
                                if (MaxPendingWriteBufferToClient > 0 && so.WritePendingBytes >= MaxPendingWriteBufferToClient)
                                {
                                    if (ServerNotification != null)
                                        ServerNotification("Dropping client: " + IPAddress.Parse(((IPEndPoint)so.workSocket.RemoteEndPoint).Address.ToString()) + " due to un-responsiveness");
                                    //Console.WriteLine(DateTime.Now.ToString() + " Dropping client: " + IPAddress.Parse(((IPEndPoint)so.workSocket.RemoteEndPoint).Address.ToString()) + " due to un-responsiveness");
                                    so.Valid = false;
                                    DisconnectClient(so);
                                    continue;
                                }

                                if (so.Valid)
                                {
                                    so.WritePendingBytes += bindata.Length;
                                    so.workSocket.BeginSend(bindata, 0, bindata.Length, 0, new AsyncCallback(SendCallback), so);
                                }
                                else
                                    DisconnectClient(so);
                            }
                            catch
                            {
                                so.Valid = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ServerNotification != null)
                            ServerNotification("Exception on SendData(): [" + data + "] " + ex.Message + " " + ex.StackTrace);
                    } 
                    finally
                    {
                        if (PublishLock.IsWriteLockHeld)
                            PublishLock.ExitWriteLock();
                    }
                }
                
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("SendData() exception: " + ex.Message);
            }

        }

        public void SendData2(string data)
        {
            try
            {                
                byte[] bindata = System.Text.Encoding.ASCII.GetBytes(data);

                IEnumerator e = connections.GetEnumerator();
                while (e.MoveNext())
                //foreach (StateObject so in connections.ToArray())
                {
                    KeyValuePair<int, StateObject> s = (KeyValuePair<int, StateObject>)e.Current;
                    StateObject so = (StateObject)s.Value; //(StateObject)e.Current;

                    try
                    {
                        if (MaxPendingWriteBufferToClient > 0 && so.WritePendingBytes >= MaxPendingWriteBufferToClient)
                        {
                            if (ServerNotification != null)
                                ServerNotification("Dropping client: " + IPAddress.Parse(((IPEndPoint)so.workSocket.RemoteEndPoint).Address.ToString()) + " due to un-responsiveness");
                            //Console.WriteLine(DateTime.Now.ToString() + " Dropping client: " + IPAddress.Parse(((IPEndPoint)so.workSocket.RemoteEndPoint).Address.ToString()) + " due to un-responsiveness");
                            so.Valid = false;
                            DisconnectClient(so);
                            continue;
                        }

                        //so.WritePendingBytes += bindata.Length;
                        //if (so.workSocket.Connected)
                        //    so.workSocket.BeginSend(bindata, 0, bindata.Length, 0, new AsyncCallback(SendCallback), so);
                        //else
                        //    DisconnectClient(so);

                        if (so.Valid)
                        {
                            so.WritePendingBytes += bindata.Length;
                            so.workSocket.BeginSend(bindata, 0, bindata.Length, 0, new AsyncCallback(SendCallback), so);
                        }
                        else
                            DisconnectClient(so);
                    }
                    catch
                    {
                        //DisconnectClient(so);
                        so.Valid = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                if (ServerNotification != null)
                    ServerNotification("SendData() exception: " + ex.Message);
            }

        }

        private void SendCallback(IAsyncResult ar)
        {
            
            StateObject so = (StateObject)ar.AsyncState;
           

            try
            {
                // Complete sending the data to the remote device.
                int bytesSent = so.workSocket.EndSend(ar);
                if(MaxPendingWriteBufferToClient > 0)
                    so.WritePendingBytes -= bytesSent;
                
            }
            catch
            {
                so.Valid = false;

                //if (ServerNotification != null)
                //    ServerNotification("SendCallback() exception: " + ex.Message);

                //DisconnectClient(so);
            }
        }


    }


}
